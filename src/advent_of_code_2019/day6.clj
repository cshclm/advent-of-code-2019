(ns advent-of-code-2019.day6
  "Day 6."
  (:require
   [clojure.string :as cstr]
   [clojure.java.io :as io]))

(def read-lines
  "Read a the lines of a file."
  (comp cstr/split-lines slurp io/file))

(defn read-input
  "Read a file."
  [i]
  (->> i read-lines (mapv #(cstr/split % #"\)"))))

(defn parse-input
  [[mass orbiter]]
  {:mass mass
   :orbiter orbiter})

(defn group-entities
  [rels]
  (group-by :mass rels))

(defn solve1
  "Solve according to requirements of part1"
  [rels]
  (let [groups (group-entities rels)]
    ((fn check [depth node]
       (let [orbiters (map :orbiter (get groups node))]
         (apply + (* (count orbiters) depth)
                (map #(check (inc depth) %) orbiters))))
     1 "COM")))

(def part1
  "Solve part1"
  #(->> "resources/day6/input"
       read-input
       (map parse-input)
       (solve1)))

(defn path-to
  [rels target]
  (let [groups (group-entities rels)]
    ((fn step [node]
       (if (= node target)
         (list node)
         (let [orbiters (map :orbiter (get groups node))]
           (if-let [match (first (filter seq (map #(step %) orbiters)))]
             (conj match node)))))
     "COM")))

(defn solve2
  "Solve according to requirements of part2"
  [rels]
  (let [to-you (rest (rest (reverse (flatten (path-to rels "YOU")))))
        to-san (rest (reverse (flatten (path-to rels "SAN"))))]
    (map first (filter (fn [[_ c]] (= c 1)) (frequencies (concat to-you to-san))))))

(def part2
  "Solve part2"
  #(->> "resources/day6/input"
       read-input
       (map parse-input)
       (solve2)
       count
       ;; the shared mass we missed
       inc))
