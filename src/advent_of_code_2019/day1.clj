(ns advent-of-code-2019.day1
  "Solutions for day 1."
  (:require
   [clojure.edn :as edn]
   [clojure.spec.alpha :as s]
   [clojure.string :as cstr]
   [clojure.java.io :as io]))

(def read-lines
  "Read a the lines of a file."
  (comp cstr/split-lines slurp io/file))

(def read-input
  "Read a file."
  #(->> % read-lines (map edn/read-string)))

(defn calc-base-fuel
  "Get the fuel to launch a given mass."
  [mass]
  (-> mass
      (/ 3)
      (Math/floor)
      (- 2)
      long))

(s/fdef calc-base-fuel
  :args (s/cat :mass pos-int?)
  :ret number?
  :fn #(<= (:ret %) (-> % :args :mass)))

(defn solve
  "Given a fuel calculation function f and some input, solve the problem."
  [f input]
  (transduce (map f) + 0 input))

(def solve1
  "Produce a solution for part 1 given some input."
  #(solve calc-base-fuel %))

(s/fdef solve1
  :args (s/cat :input (s/coll-of int?))
  :ret int?)

(def part1
  "Solve part 1."
  #(-> "resources/day1/input" read-input solve1))

(def calc-base-fuel2
  "Calculate the base fuel for part 2."
  (comp #(max 0 %) calc-base-fuel))

(defn meta-fuel
  "Get the required fuel for the fuel."
  [fuel]
  (let [mf (calc-base-fuel2 fuel)]
    (if (zero? mf)
      fuel
      (+' fuel (meta-fuel mf)))))

(s/fdef meta-fuel
  :args (s/cat :fuel pos-int?)
  :ret number?
  :fn #(>= (:ret %) (-> % :args :fuel)))

(defn solve2
  "Produce a solution for part 2 given some input."
  [input]
  (solve (comp meta-fuel calc-base-fuel2) input))

(s/fdef solve2
  :args (s/cat :input (s/coll-of int?))
  :ret int?
  :fn #(>= (:ret %) 0))

(def part2
  "Solve part 2."
  #(-> "resources/day1/input" read-input solve2))
