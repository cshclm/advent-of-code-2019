(ns advent-of-code-2019.day8
  "Solutions for day 8."
  (:require
   [clojure.string :as cstr]
   [clojure.java.io :as io]))

(def read-input
  "Read a the lines of a file."
  (comp #(partition (* 25 6) %)
        seq
        slurp
        io/file))

(defn num-ns
  [n coll]
  (count (filter #{n} coll)))

(defn solve1
  [coll]
  (let [layer (apply min-key #(num-ns \0 %) coll)]
    (* (num-ns \1 layer) (num-ns \2 layer))))

(def part1
  "Solve part 1."
  #(-> "resources/day8/input" read-input solve1))

(defn render
  [layer]
  (->> layer
       (map #(if (= \0 %) " " "#"))
       (partition 25)
       (map (partial apply str))
       (cstr/join "\n")))

(defn top-value
  [all-layers pos]
  (->> all-layers
       (map #(nth % pos))
       (drop-while #{\2})
       first))

(defn solve2
  [all-layers]
  (->> (range 150)
       (map #(top-value all-layers %))
       render
       println))

(def part2
  "Solve part 2."
  #(-> "resources/day8/input" read-input solve2))
