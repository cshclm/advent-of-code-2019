(ns advent-of-code-2019.util.vm
  (:require
   [clojure.spec.alpha :as s]))

(def read-lengths
  {0 0
   1 3
   2 3
   3 1
   4 1
   5 2
   6 2
   7 3
   8 3
   99 0})

(s/def ::opcode (set (keys read-lengths)))

(s/def ::value int?)

(s/def ::mode #{::immediate ::position})

(s/def ::parameter
  (s/keys :req-un [::mode ::value]))

(s/def ::parameters
  (s/coll-of ::parameter :kind vector? :min-count 0 :max-count 3))

(s/def ::mem
  (s/coll-of ::opcode :kind vector? :min-count 4))

(s/def ::instruction
  (s/keys :req-un [::parameters ::opcode]))

(s/def ::address
  (set (range 100)))

(defn op-modes
  [ps]
  (mapv #(condp = %
           \1 ::immediate
           \0 ::position)
        ps))

(defn pad
  [coll]
  (concat coll (repeat 4 ::position)))

(defn operation-at
  [iptr mem]
  (let [code (str (nth mem iptr))
        len (count code)
        instruction (Integer/parseInt (apply str (drop (- len 2) code)))
        modes (pad (reverse (op-modes (take (- len 2) code))))]
    {:opcode instruction
     :modes modes}))

(defn parameters
  [iptr mem instruction]
  (->> mem
       (drop (inc iptr))
       (take (get read-lengths instruction))
       vec))

(defn instruction-at
  "Return the instruction at iptr"
  [iptr mem]
  (let [operation (operation-at iptr mem)
        ps (parameters iptr mem (:opcode operation))]
    (assoc operation :parameters
           (mapv #(hash-map :value %1 :mode %2) ps (:modes operation)))))

(s/fdef instruction-at
  :args (s/and (s/cat :iptr ::address :mem ::mem)
               #(< (:iptr %) (count (:mem %))))
  :ret ::instruction
  :fn (s/and #(java.util.Collections/indexOfSubList (-> % :args :mem)
                                                    (:parameters (:ret %)))
             #(= (nth (-> % :args :mem) (-> % :args :iptr))
                 (:opcode (:ret %)))))

(defn op-value
  [mem {:keys [value mode]}]
  (condp = mode
    ::position (get mem value)
    ::immediate value
    (throw (ex-info "Unknown mode" {}))))

(defn step-iptr
  [{:keys [parameters]} iptr]
  (+ iptr (inc (count parameters))))

(defmulti step* (fn [i _ _ _] (:opcode i)))
(defmethod step* 1
  [{:keys [parameters] :as instruction} iptr mem _]
  (let [[op1 op2 res] parameters]
    {:next-mem (->> [op1 op2]
               (map #(op-value mem %))
               (apply +')
               (assoc mem (:value res)))
     :next-iptr (step-iptr instruction iptr)}))

(defmethod step* 2
  [{:keys [parameters] :as instruction} iptr mem _]
  (let [[op1 op2 res] parameters]
    {:next-mem (->> [op1 op2]
               (map #(op-value mem %))
               (apply *')
               (assoc mem (:value res)))
     :next-iptr (step-iptr instruction iptr)}))

(defmethod step* 3
  [{:keys [parameters] :as instruction} iptr mem input]
  (let [[res] parameters
        v (first input)]
    (if v
      {:next-mem (assoc mem (:value res) v)
       :next-iptr (step-iptr instruction iptr)
       :next-input (rest input)}
      {:state ::yield})))

(defmethod step* 4
  [{:keys [parameters] :as instruction} iptr mem _]
  (let [[op1] parameters]
    {:next-iptr (step-iptr instruction iptr)
     :new-output (op-value mem op1)}))

(defmethod step* 5
  [{:keys [parameters] :as instruction} iptr mem _]
  (let [[op1 op2] parameters]
    (if (pos-int? (op-value mem op1))
      {:next-iptr (op-value mem op2)}
      {:next-iptr (step-iptr instruction iptr)})))

(defmethod step* 6
  [{:keys [parameters] :as instruction} iptr mem _]
  (let [[op1 op2] parameters]
    (if (zero? (op-value mem op1))
      {:next-iptr (op-value mem op2)}
      {:next-iptr (step-iptr instruction iptr)})))

(defmethod step* 7
  [{:keys [parameters] :as instruction} iptr mem _]
  (let [[op1 op2 res] parameters]
    {:next-mem (assoc mem (:value res)
                     (if (< (op-value mem op1) (op-value mem op2))
                       1 0))
     :next-iptr (step-iptr instruction iptr)}))

(defmethod step* 8
  [{:keys [parameters] :as instruction} iptr mem _]
  (let [[op1 op2 res] parameters]
    {:next-mem (assoc mem (:value res)
                     (if (= (op-value mem op1) (op-value mem op2))
                       1 0))
     :next-iptr (step-iptr instruction iptr)}))

(defmethod step* 99
  [_ _ _ _]
  {:state ::halt})

(defmethod step* :default
  [i _ _ _]
  (throw (ex-info "Illegal opcode" (select-keys i [:opcode]))))

(defn step
  [iptr mem input]
  (step* (instruction-at iptr mem) iptr mem input))

(defn execute
  "Run the virtual machine"
  ([init-iptr init-mem]
   (execute init-iptr init-mem '()))
  ([init-iptr init-mem init-input]
   (loop [iptr init-iptr mem init-mem input init-input output []]
     (let [{:keys [next-mem next-iptr next-input new-output state]} (step iptr mem input)
           next-output (if new-output (conj output new-output) output)]
       (condp = state
         ::halt {:mem (or next-mem mem)
                 :output next-output}
         ::yield {:iptr (or next-iptr iptr)
                  :mem (or next-mem mem)
                  :input (or next-input input)
                  :output next-output}
         (recur (or next-iptr iptr)
                (or next-mem mem)
                (or next-input input)
                next-output))))))
