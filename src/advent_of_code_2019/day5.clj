(ns advent-of-code-2019.day5
  "Day 5."
  (:require
   [advent-of-code-2019.util.vm :as vm]
   [clojure.string :as cstr]
   [clojure.java.io :as io]))

(def user-input 1)

(def read-lines
  "Read a the lines of a file."
  (comp #(cstr/split % #",") slurp io/file))

(defn read-input
  "Read a file."
  [s]
  (->> s read-lines (mapv #(Integer/parseInt ^String (cstr/trim %)))))

(defn solve
  [user-input]
  (->> user-input
       list
       (vm/execute 0 (read-input "resources/day5/input"))
       :output))

(defn part1
  []
  (solve 1))

(defn part2
  []
  (solve 5))
