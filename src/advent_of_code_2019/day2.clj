(ns advent-of-code-2019.day2
  "Day 2."
  (:require
   [advent-of-code-2019.util.vm :as vm]
   [clojure.math.combinatorics :as com]
   [clojure.string :as cstr]
   [clojure.java.io :as io]
   [clojure.edn :as edn]))

(def read-lines
  "Read a the lines of a file."
  (comp #(cstr/split % #",") slurp io/file))

(def read-input
  "Read a file."
  #(->> % read-lines (mapv edn/read-string)))

(defn init-mem
  "Initialise the memory."
  [noun verb mem]
  (-> mem
      (assoc 1 noun)
      (assoc 2 verb)))

(defn solve1
  "Solve according to requirements of part1"
  [iptr mem]
  (-> (vm/execute iptr mem) :mem first))

(def part1
  "Solve part1"
  #(->> "resources/day2/input"
        read-input
        (init-mem 12 2)
        (solve1 0)))

(def goal-value
  "Value sought after for part 2."
  19690720)

(defn solve2
  "Solve according to requirements of part2"
  [input]
  (->> (range 0 100)
       (repeat 2)
       (apply com/cartesian-product)
       (filter (fn [[noun verb]]
                 (= goal-value (solve1 0 (init-mem noun verb input)))))
       first))

(def part2
  "Solve part 2"
  #(->> "resources/day2/input" read-input solve2))
