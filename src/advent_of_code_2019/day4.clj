(ns advent-of-code-2019.day4
  "Day 4."
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]))

(def input "347312-805915")

(def parse-input
  "Read a file."
  #(mapv edn/read-string (cstr/split % #"-")))

(defn range-incl
  [x y]
  (range x (inc y)))

(def problem-range
  #(apply range-incl (take 2 %)))

(defn two-adjacent?
  [n]
  (->> n
       str
       (partition-by identity)
       (filter #(>= (count %) 2))
       first
       seq?))

(defn increasing?
  [n]
  (let [ds (->> n
                str
                seq
                (mapv (comp edn/read-string str)))]
    (= ds (sort ds))))

(defn n-satisfies?
  [n]
  (and (two-adjacent? n)
       (increasing? n)))

(defn solve1
  "Solve according to requirements of part1"
  [input]
  (filter n-satisfies? input))

(def part1
  "Solve part1"
  #(-> input
       (parse-input)
       problem-range
       solve1
       count))

(defn n-satisfies2?
  [n]
  (let [groups (partition-by identity (str n))
        doubles (map first (filter #(= (count %) 2) groups))
        gdigs (frequencies (map first groups))]
    (prn gdigs)
    (seq? (seq (filter #(= (gdigs %) 1) doubles)))))

(defn solve2
  [input]
  (filter n-satisfies2? (solve1 input)))

(def part2
  "Solve part2"
  #(-> input
       (parse-input)
       problem-range
       solve2
       count))
