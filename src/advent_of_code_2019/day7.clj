(ns advent-of-code-2019.day7
  "Day 7."
  (:require
   [clojure.math.combinatorics :as com]
   [advent-of-code-2019.util.vm :as vm]
   [clojure.string :as cstr]
   [clojure.java.io :as io]))

(def user-input 1)

(def read-lines
  "Read a the lines of a file."
  (comp #(cstr/split % #",") slurp io/file))

(defn read-input
  "Read a file."
  [s]
  (->> s read-lines (mapv #(Integer/parseInt ^String (cstr/trim %)))))

(defn perms
  []
  (com/permutations [0 1 2 3 4]))

(def code
  (->> "resources/day7/input" read-input))

(defn solve
  [phases]
  (loop [rem-phases phases
         last-input '(0)]
    (if (seq (rest rem-phases))
      (let [v (vm/execute 0 code (conj last-input (first rem-phases)))]
        (recur (rest rem-phases) (into '() (:output v))))
      (first (:output (vm/execute 0 code (conj last-input (first rem-phases))))))))

(defn perms2
  []
  (com/permutations [5 6 7 8 9]))

(defn part1
  []
  (->> (perms)
       (map solve)
       (apply max)))

(defn boot-up
  [phases]
  (loop [machines []
         rem-phases phases
         last-output '(0)]
    (if (seq rem-phases)
      (let [v (vm/execute 0 code (list (first rem-phases) (first last-output)))]
        (recur (conj machines v) (rest rem-phases) (into '() (:output v))))
      machines)))

(defn reschedule
  [machines cont]
  (if (:iptr cont)
    (conj (vec (next machines)) cont)
    (next machines)))

(defn feedback
  [machines]
  (loop [machines machines
         last-output (into '() (:output (last machines)))]
    (if-let [m (first machines)]
      (let [cont (vm/execute (:iptr m) (:mem m) last-output)]
        (recur (reschedule machines cont) (:output cont)))
      (first last-output))))

(defn part2
  []
  (->> (perms2)
       (map (comp feedback boot-up))
       (apply max)))
