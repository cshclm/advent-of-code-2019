(ns advent-of-code-2019.day3
  "Day 3."
  (:require
   [clojure.math.combinatorics :as com]
   [clojure.string :as cstr]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [clojure.set :as set]))

(def read-lines
  "Read a the lines of a file."
  (comp cstr/split-lines slurp io/file))

(def read-direction
  (comp keyword str first))

(def read-distance
  (comp edn/read-string #(apply str %) rest))

(def partition-span
  (juxt read-direction read-distance))

(defn plane
  [d]
  (if (#{:U :D} d) :y :x))

(defn span-distance
  [d n]
  (if (#{:U :L} d) (* -1 n) n))

(defn parse-span
  [[d n]]
  {:plane (plane d)
   :distance (span-distance d n)})

(defmulti next-pos (fn [v _] (:plane v)))
(defmethod next-pos :x
  [{:keys [distance]} [x y]]
  [(+ x distance) y])
(defmethod next-pos :y
  [{:keys [distance]} [x y]]
  [x (+ y distance)])

(def read-span
  (comp parse-span partition-span))

(defn read-wire
  [s]
  (mapv read-span (cstr/split s #",")))

(def read-input
  "Read a file."
  #(->> % read-lines (mapv read-wire)))

(defn plot-points
  [w]
  (reduce (fn [acc x] (conj acc (next-pos x (last acc)))) [[0 0]] w))

(defn coord-range
  [n1 n2]
  (if (= n2 n1)
    [n1]
    (range n1 n2
           (if (< n2 n1) -1 1))))

(defn explode-pair
  [[x1 y1] [x2 y2]]
  (com/cartesian-product (coord-range x1 x2) (coord-range y1 y2)))

(defn explode-points
  [ps]
  (mapcat explode-pair ps (rest ps)))

(defn distance-from-port
  [pt]
  (apply + (map #(Math/abs %) pt)))

(defn steps
  [input]
  (map (comp explode-points plot-points) input))

(defn intersections
  [sts]
  (disj (apply set/intersection (map #(into #{} %) sts)) '(0 0)))

(defn solve1
  "Solve according to requirements of part1"
  [input]
  (first (sort-by distance-from-port (intersections (steps input)))))

(def part1
  "Solve part1"
  #(-> "resources/day3/input"
       read-input
       (solve1)))

(defn steps-to-reach
  [sec path]
  (count (take-while #(not= % sec) path)))

(defn solve2
  "Solve according to requirements of part1"
  [input]
  (let [sts (steps input)
        secs (intersections sts)]
    (sort-by second
     (map
      (fn [sec] [sec (apply + (map #(steps-to-reach sec %) sts))])
      secs))))

(def part2
  "Solve part2"
  #(-> "resources/day3/input"
       read-input
       (solve2)))
