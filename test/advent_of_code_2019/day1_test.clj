(ns advent-of-code-2019.day1-test
  (:require  [clojure.test :refer [deftest testing are]]
             [advent-of-code-2019.day1 :as sut]))

(deftest solve1-test
  (testing "solve1"
    (are [input expected] (= expected (sut/solve1 input))
      [12] 2
      [14] 2
      [1969] 654
      [100756] 33583
      [12 14] 4
      [] 0
      [0] -2)))

(deftest solve2-test
  (testing "solve2"
    (are [input expected] (= expected (sut/solve2 input))
      [12] 2
      [14] 2
      [1969] 966
      [100756] 50346
      [12 14] 4
      [] 0
      [0] 0)))
